import java.util.*;
class test_4
{
static int stars=1;
public static void main(String v[]){
Scanner key=new Scanner(System.in);
System.out.print("Enter n : ");
int n=Integer.parseInt(key.nextLine());
int loop = (2*n)-1;
for(int i=1;i<=loop;i++)
{
	String msg=new String();
	if(i<n)
	{
		msg=test_4.printUpper(i,loop);
	}
	else
	{	
		msg=test_4.printLower(i,loop);

	}
	System.out.println(msg);
}
}
static String printLower(int n,int loop)
{
	String msg="";
	int space=(loop-test_4.stars)/2;
	for(int j=0;j<space;j++)
		msg+=" ";
	for(int j=0;j<stars;j++)
		msg+="*";
	for(int j=0;j<space;j++)
		msg+=" ";
	test_4.stars-=2;
	return (msg);
}
static String printUpper(int n,int loop)
{
	String msg="";
	int space=(loop-stars)/2;
	for(int j=0;j<space;j++)
		msg+=" ";
	for(int j=0;j<stars;j++)
		msg+="*";
	for(int j=0;j<space;j++)
		msg+=" ";
	stars+=2;
	return (msg);
}
}
