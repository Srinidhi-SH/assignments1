import java.util.*;

class test_7{
public static void main(String v[]){
	test_7 t=new test_7();
	System.out.println("Enter n");
	int n=Integer.parseInt(new Scanner(System.in).nextLine());
	ArrayList<Integer> a = new ArrayList<Integer>();
	a=t.generate(n);
	for(int i=0;i<a.size();i++)
	{
		System.out.print(a.get(i)+" ");
	}
}

boolean isPrime(int n)
{
	for(int i=n-1;i>1;i--)
	{
	        if( ( n % i)==0 )
	                return false;
	}
	return true;
}

ArrayList<Integer> generate(int n)
{
	ArrayList<Integer> m=new ArrayList<Integer>();
	for(int i=2;i<n;i++)
	{
	        if((n%i)==0)
	                if(isPrime(i))
	                        m.add(i);
	}
	return(m);
}
}
